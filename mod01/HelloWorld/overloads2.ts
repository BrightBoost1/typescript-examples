function add1(x: string, y: number): string;
function add1(x: number, y: string): number;

function add1(x: any, y: any): any {
  return x + y;
}

console.log(add1("str", 3));
console.log(add1(2, "str"));

