function add(x: string, y: number): number;
function add(x: number, y: number, z: number): number;

function add(x: any, y: number, z: number = 0): any {
  return x + y + z;
}

console.log(add("2", 3));
console.log(add(2, 3, 1));

let x1 = add("4", 5);