class Person {
  name: string;
  age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }
}

class Employee extends Person {
  employeeNr: string;

  constructor(name: string, age: number, employeeNr: string) {
    super(name, age);
    this.employeeNr = employeeNr;
  }
}

// function print1(person: Person): void;
// function print1(person: Employee): void;

function print1(person: Person): void {
  if (person instanceof Employee) {
    console.log(person.employeeNr);
  } else {
    console.log(person.name);
    // console.log(person.employeeNr); // it knows it's not an Employee, this is not allowed
  }
}

let emp: Employee = new Employee("bobby", 1, "123");
let person: Person = new Person("mary", 25);

print1(emp);
print1(person);
